﻿Imports System.IO : Imports System.Net
Module Module1
    REM  JP2GMD++ is (C) 2008-2017 drmrvr12
    REM  This software is provided AS-IS, without any warranty
    Sub Main1(FlName As String)
        Dim fiel As String()
        Dim var(16386) As String
        fiel = File.ReadAllText(FlName).Split(New Char() {vbLf, "|"})
        Dim dlmt As Char = "|"
        For Each input As String In fiel
            If input.StartsWith("Rem") Then : End If
            If input.StartsWith("VAR Rem") Then : End If
            If input.StartsWith("ECHO ") Then
                Wriet(input.Substring(5))
            End If
            If input.StartsWith("SLEEP ") Then
                System.Threading.Thread.Sleep(input.Substring(6))
            End If
            If input.StartsWith("VAR SLEEP ") Then
                System.Threading.Thread.Sleep(var(input.Substring(10)))
            End If
            If input.StartsWith("WRIET ") Then
                Dim senpai() As String = input.Substring(6).Split(New Char() {"^"})
                System.IO.File.AppendAllText(senpai(0), senpai(1))
            End If
            If input.StartsWith("VAR WRIET ") Then
                Dim senpai() As String = input.Substring(10).Split(New Char() {"^"})
                System.IO.File.AppendAllText(var(senpai(0)), var(senpai(1)))
            End If
            If input.StartsWith("WRIET-LIEN ") Then
                Dim senpai() As String = input.Substring(11).Split(New Char() {"^"})
                System.IO.File.AppendAllText(senpai(0), Environment.NewLine + senpai(1))
            End If
            If input.StartsWith("VAR WRIET-LIEN ") Then
                Dim senpai() As String = input.Substring(15).Split(New Char() {"^"})
                System.IO.File.AppendAllText(var(senpai(0)), Environment.NewLine + var(senpai(1)))
            End If
            If input.StartsWith("DAUNLOUD ") Then '9
                Dim url As String = input.Substring(9)
                My.Computer.Network.DownloadFile(url, Path.GetFileName(url))
            End If
            If input.StartsWith("VAR DAUNLOUD ") Then '9
                Dim url As String = var(input.Substring(13))
                My.Computer.Network.DownloadFile(url, Path.GetFileName(url))
            End If
            If input.StartsWith("DAUNLOUDZ ") Then '10
                Dim url() As String = input.Substring(10).Split(New Char() {"^"})
                My.Computer.Network.DownloadFile(url(0), url(1))
            End If
            If input.StartsWith("VAR DAUNLOUDZ ") Then '10
                Dim url() As String = input.Substring(10).Split(New Char() {"^"})
                My.Computer.Network.DownloadFile(var(url(0)), var(url(1)))
            End If
            If input.StartsWith("DECLARE ME SENPAI ") Then '18
                Dim senpai() As String = input.Substring(18).Split(New Char() {"^"})
                var(senpai(0)) = senpai(1)
            End If
            If input.StartsWith("VAR DECLARE ME SENPAI ") Then '18
                Dim senpai() As String = input.Substring(22).Split(New Char() {","})
                var(senpai(0)) = var(senpai(1))
            End If
            If input.StartsWith("READ ME SENPAI ") Then '15
                Dim senpai() As String = input.Substring(18).Split(New Char() {"^"})
                var(input.Substring(15)) = Console.ReadLine
            End If
            If input.StartsWith("VAR ECHO ") Then
                Wriet(var(input.Substring(9)))
            End If
            If input.StartsWith("MORE ") Then
                Wriet(System.IO.File.ReadAllText(input.Substring(5)))
            End If
            If input.StartsWith("MORE AS VAR ") Then
                Dim senpai() As String = input.Substring(12).Split(New Char() {"^"})
                var(senpai(0)) = System.IO.File.ReadAllText(senpai(1))
            End If
            If input.StartsWith("VAR MORE AS VAR ") Then
                Dim senpai() As String = input.Substring(16).Split(New Char() {"^"})
                var(senpai(0)) = System.IO.File.ReadAllText(var(senpai(1)))
            End If
            If input.StartsWith("REF ") Then
                Main1(input.Substring(4))
            End If
            If input.StartsWith("VAR REF ") Then
                Main1(var(input.Substring(8)))
            End If
            If input.StartsWith("START ") Then
                System.Diagnostics.Process.Start(input.Substring(5))
            End If
            If input.StartsWith("VAR START ") Then
                System.Diagnostics.Process.Start(var(input.Substring(9)))
            End If
            If input.StartsWith("FIEL MOVE ") Then '10
                Dim senpai() As String = input.Substring(10).Split(New Char() {"^"})
                File.Move(senpai(0), senpai(1))
            End If
            If input.StartsWith("FIEL COPY ") Then '10
                Dim senpai() As String = input.Substring(10).Split(New Char() {"^"})
                File.Copy(senpai(0), senpai(1))
            End If
            If input.StartsWith("FIEL DELIT ") Then '11
                File.Delete(input.Substring(11))
            End If
            If input.StartsWith("VAR FIEL MOVE ") Then '14
                Dim senpai() As String = input.Substring(14).Split(New Char() {"^"})
                File.Move(var(senpai(0)), var(senpai(1)))
            End If
            If input.StartsWith("VAR FIEL COPY ") Then '14
                Dim senpai() As String = input.Substring(14).Split(New Char() {"^"})
                File.Copy(var(senpai(0)), var(senpai(1)))
            End If
            If input.StartsWith("VAR FIEL DELIT ") Then '15
                File.Delete(var(input.Substring(15)))
            End If
            If input.StartsWith("TITLE ") Then '6
                Console.Title = input.Substring(6)
            End If
            If input.StartsWith("VAR TITLE ") Then '10
                Console.Title = var(input.Substring(10))
            End If
            If input.StartsWith("PAUSE") Then
                Console.ReadKey()
            End If
            If input.StartsWith("IF EQUALS ") Then
                Dim senpai As String() = input.Substring(10).Split(","c)
                If senpai(0) = senpai(1) Then
                    Main1(senpai(2))
                End If
            End If

            If input.StartsWith("IF NOT EQUALS ") Then
                Dim senpai As String() = input.Substring(14).Split(","c)
                If senpai(0) <> senpai(1) Then
                    Main1(senpai(2))
                End If
            End If

            If input.StartsWith("IF EQUAL ") Then
                Dim senpai As String() = input.Substring(9).Split(","c)
                If senpai(0) = senpai(1) Then
                    Main1(senpai(2))
                End If
            End If

            If input.StartsWith("IF NOT EQUAL ") Then
                Dim senpai As String() = input.Substring(13).Split(","c)
                If senpai(0) <> senpai(1) Then
                    Main1(senpai(2))
                End If
            End If

            If input.StartsWith("IF EXIST ") Then
                Dim senpai As String() = input.Substring(9).Split(","c)
                If File.Exists(senpai(0)) Then
                    Main1(senpai(1))
                End If
            End If

            If input.StartsWith("IF EXISTS ") Then
                Dim senpai As String() = input.Substring(10).Split(","c)
                If File.Exists(senpai(0)) Then
                    Main1(senpai(1))
                End If
            End If

            If input.StartsWith("IF NOT EXIST ") Then
                Dim senpai As String() = input.Substring(13).Split(","c)
                If Not File.Exists(senpai(0)) Then
                    Main1(senpai(1))
                End If
            End If

            If input.StartsWith("IF NOT EXISTS ") Then
                Dim senpai As String() = input.Substring(14).Split(","c)
                If Not File.Exists(senpai(0)) Then
                    Main1(senpai(1))
                End If
            End If
        Next
    End Sub
    Sub Wriet(S As String)
        System.Console.WriteLine(S)
    End Sub
    Sub WrietS(S As String)
        System.Console.Write(S)
    End Sub
    Sub Main()
        Try
            Main1(My.Application.CommandLineArgs(0))
        Catch ex As Exception
            Dim title() As String = {"Oh no!", "Damn it!", "Crap!", "Ayayay", "Ooi", "Something went wrong", "Oh", "Shit!", "Aaaaaaa", "Not the exception!", "Maybe ; is missing?"}
            Dim rnd As Random = New Random
            Console.Title = title(rnd.Next(0, title.Length))
            Wriet("Try ... Catch Exception")
            Dim ctc() As String = ex.ToString.Split(New Char() {vbLf})
            Wriet(ctc(0))
            Wriet("End Try")
            Console.ReadKey()
        End Try

    End Sub
End Module
