What?
===================================================
JP2GMD++ is High-level, non-object-oriented, easy to learn, hard-to-read Interpreter programming language.

Good-to-know
===================================================
White spaces are important - you should not start a line with'em

If you want to have two functions on same line you shall use "|"
```
ECHO That's the first function|ECHO And that's another function in same line!!1
```
If you are dealing with filenames it is recommendewd to end line with "|"

Variable names may contain ONLY numbers and can't be greater than 16386

Hello world
===================================================
```
Rem Echo just returns plain text
ECHO Hello world!
PAUSE
```

Save to file \ read text from file
===================================================
```
WRIET test.txt^Hello world!
MORE test.txt
PAUSE
```
Reference to another code file
===================================================
```
REF anotherfile.extension
PAUSE
```
Variables
===================================================
```
DECLARE ME SENPAI 21^Enter time to sleep:
VAR ECHO 21
READ ME SENPAI 22
VAR SLEEP 22
ECHO Dun
PAUSE
```

Comparsion with batch
====================================================
JP2GMD++:
-------------
```
DECLARE ME SENPAI 212^Write some text....
ECHO Hello world~!|VAR ECHO 212|
READ ME SENPAI 22|
ECHO Now, where to save this?
READ ME SENPAI 23|
WRIET-LIEN 23,22|
ECHO Dun!
PAUSE
```
Batch:
-----------
```
SET 212=Write some text...
ECHO Hello world~!&&ECHO %212%
SET /P 22=
ECHO Now, where to save this?
SET /P 23=
ECHO %23%>>%22%
ECHO Dun!
PAUSE
```


JP2GMD++ syntax was created through 2008-2017 by drmrvr12

This program was written in 2017 by drmrvr12, it's open source and provided AS-IS
